#!/usr/bin/env node
"use strict"

const os = require('os')
const process = require('process')
const path = require('path')
const node_modules = path.join(__dirname, '..', 'node_modules')
const fontBuild = require(path.join(node_modules, 'mdi-font-build'))

let icons = [
  'label', 'update', 'note-plus', 'chevron-up', 'chevron-down',
  'menu', 'home', 'information', 'link', 'rss', 'file-image', 'loading'
]

const extra = path.join(__dirname, 'extra-icons/*.svg')

if (require.main === module) {
  icons = icons.concat(process.argv.slice(2))
  fontBuild(icons, [extra], process.env['WEBFONT_DIR'])
}
