import os
import configparser
import mistune
import re
from api import thumbnail


def get_ini_default_section(ini: str):
    cp = configparser.ConfigParser()
    cp.read_string('[DEFAULT]\n' + ini)
    return cp['DEFAULT']


def remove_tags(text):
    return re.sub('<[^ ][^>]*>', '', text)


class BriefGetter(mistune.HTMLRenderer):
    def __init__(self, *args, **kwargs):
        self.paragraph_count = 4
        self.result = {'title': None, 'image': None, 'brief': ''}
        self.config = None
        super(BriefGetter, self).__init__(*args, **kwargs)

    def block_code(self, code, lang=None):
        if lang == 'page.ini':
            self.config = code
        return super().block_code(code, lang)

    def paragraph(self, text):
        if self.paragraph_count:
            text = remove_tags(text)
            if len(text):
                self.result['brief'] += '<p>' + mistune.escape(text) + '</p>'
                self.paragraph_count -= 1
        return super().paragraph(text)

    def image(self, src, title, text):
        if not self.result['image']:
            self.result['image'] = src
        return super().image(src, title, text)

    def header(self, text, level, raw=None):
        if level == 1 and not self.result['title']:
            self.result['title'] = remove_tags(text)
        return super().header(text, level, raw)


def getcards(files):
    """Returns [{title: str, type: ('blog'|'note'), link: str, cols: int},...]"""
    cardlist = []
    for filename in files:
        with open(filename) as f:
            raw_md = f.read()
        brief_getter = BriefGetter()
        parse = mistune.Markdown(renderer=brief_getter)
        parse(raw_md)
        result = brief_getter.result

        dash_title = filename.split(os.sep)[-1].split('.md')[0]
        if not result['title']:
            a = []
            for i in dash_title.split('-'):
                a.append(i.capitalize())
            result['title'] = ' '.join(a)

        if brief_getter.config:
            ini = get_ini_default_section(brief_getter.config)
            if 'title' in ini:
                result['title'] = ini['title']

            if 'created' in ini:
                result['created'] = int(ini['created'])
            if 'modified' in ini:
                result['modified'] = int(ini['modified'])

            # Redirect this card to other URL
            if 'redirect' in ini:
                result['redirect'] = ini['redirect']

        if not 'created' in result:
            result['created'] = get_created_time_via_git(filename)

        if not 'modified' in result:
            result['modified'] = get_modified_time_via_git(filename)

        result['type'] = filename.split(os.sep)[-2]
        result['id'] = dash_title
        result['cols'] = 8 if result['type'] == 'blog' else 4

        result['thumbnail'] = thumbnail.thumbnail(result['image'])

        cardlist.append(result)
    cardlist.sort(key=lambda x: x['modified'], reverse=True)
    return cardlist


import subprocess


def get_modified_time_via_git(filename):
    p = subprocess.Popen(['git', 'log', '-1', '--format=%at', '--', filename],
                         stdout=subprocess.PIPE,
                         stdin=subprocess.PIPE)
    time_byte, _ = p.communicate()
    return int(time_byte.decode().strip('\n'))  # remove '\n'


def get_created_time_via_git(filename):
    p = subprocess.Popen([
        'git', 'log', '--diff-filter=A', '--follow', '--format=%at', '--',
        filename
    ],
                         stdout=subprocess.PIPE,
                         stdin=subprocess.PIPE)
    time_byte, _ = p.communicate()
    # A file may be added for multiple times, but we want the first one
    return int(time_byte.decode().split('\n')[0])


import glob


def cards():
    return getcards(glob.glob('./**/*.md'))
