from PIL import Image
import requests
import base64
import os
from io import BytesIO
from .constants import CACHE_DIRECTORY

os.makedirs(CACHE_DIRECTORY, exist_ok=True)

HEADERS = {
    'User-Agent':
    'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) ' +
    'AppleWebKit/537.36 (KHTML, like Gecko) ' +
    'Chrome/67.0.3396.87 Mobile Safari/537.36'
}


def thumbnail(url):
    if url is None:
        return None
    if os.path.splitext(url)[-1] not in ['.png', '.jpg']:
        return None

    file_location = os.path.join(
        CACHE_DIRECTORY, base64.urlsafe_b64encode(url.encode()).decode())

    if not os.path.isfile(file_location):
        print('Generating image thumbnails...')
        r = requests.get(url, headers=HEADERS)
        with open(file_location, 'wb') as f:
            f.write(r.content)

    buffer = BytesIO()
    im = Image.open(file_location)
    im.thumbnail((16, 16), Image.ANTIALIAS)
    im.save(buffer, format='PNG')
    encoded = base64.b64encode(buffer.getvalue()).decode()
    if len(encoded) > 2000:
        return None
    return encoded
