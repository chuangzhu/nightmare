"""
Nightmare backend server.

A GitLab webhooks receiver is included in this program.
Please set up webhooks at <your post repo> -> Settings -> Webhooks.
Then pass the token through `GITLAB_WEBHOOK_TOKEN` environ.
"""

import tornado.ioloop
import tornado.web
import subprocess
import json
import toml
import os
from . import cards
from .constants import CACHE_DIRECTORY


class WebfontError(Exception):
    pass


class ServerBuffer:
    WEBFONTJS = os.path.join(os.path.dirname(__file__), 'webfont.js')
    WEBFONT_DIR = os.path.join(CACHE_DIRECTORY, 'webfont')

    def __init__(self):
        self.buffer = {}
        self.refresh()
        print('> Serving at 0.0.0.0:5000')

    def __getitem__(self, key):
        return self.buffer[key]

    def __contains__(self, key):
        return key in self.buffer

    def refresh(self):
        config = toml.load('./config.toml')
        self.buffer.update({
            'cards': json.dumps(cards.cards()),
            'config': json.dumps(config),
            'about': json.dumps(toml.load('./about.toml')),
            'links': json.dumps(toml.load('./links.toml'))
        })
        icons = list(config['owner']['social'])
        os.environ.update({'WEBFONT_DIR': self.WEBFONT_DIR})
        p = subprocess.run(['node', self.WEBFONTJS] + icons, env=os.environ)
        if p.returncode:
            raise WebfontError


global_buffer = ServerBuffer()


class RequestHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Methods', 'GET')
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate')


class StaticFileHandler(tornado.web.StaticFileHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Methods', 'GET')
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate')


class MainHandler(RequestHandler):
    def get(self):
        self.write('Hello!')


class JsonHandler(RequestHandler):
    def get(self, name):
        if name not in global_buffer:
            return
        self.write(global_buffer[name])
        self.set_header('Content-Type', 'application/json')


class WebhookHandler(RequestHandler):
    def post(self):
        token = os.environ.get('GITLAB_WEBHOOK_TOKEN')
        if not token:
            self.set_status(404)
            self.write('Webhook not set')
            return
        if self.request.headers['X-Gitlab-Token'] == token:
            subprocess.run(['git', 'pull'])
            global_buffer.refresh()
            print('Posts repo updated.')
            self.write('OK')
            return
        self.set_status(401)
        self.write('Invalid token')


def make_app():
    return tornado.web.Application([
        (r'/', MainHandler),
        (r'/(\w+)\.json', JsonHandler),
        (r'/webhook', WebhookHandler),
        (r'/webfont/(.*)', StaticFileHandler, {
            'path': global_buffer.WEBFONT_DIR
        }),
        (r'/note/(.*)', StaticFileHandler, dict(path='./note/')),
        (r'/blog/(.*)', StaticFileHandler, dict(path='./blog/')),
    ])


def main():
    app = make_app()
    app.listen(5000)
    tornado.ioloop.IOLoop.current().start()
