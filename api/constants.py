import os

CACHE_DIRECTORY = os.path.join(os.environ.get('CACHE_DIRECTORY') or
                               os.environ.get('XDG_CACHE_HOME') or
                               os.path.expanduser('~/.cache'),
                               'nightmare-api')
