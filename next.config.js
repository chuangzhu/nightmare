const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')
const webpack = require('webpack')
const fetch = require('isomorphic-unfetch')

const PRODUCTION = process.env.NODE_ENV === 'production'

if (PRODUCTION && process.env.APIURL === undefined) {
  console.error('Environment variable `APIURL` not set.')
  process.exit(1)
}

const _APIURL = process.env.APIURL || 'http://localhost:5000'
const SSR_API = process.env.SSR_API || _APIURL
const CSR_API = process.env.CSR_API || _APIURL

async function exportPathMap() {
  const fetchedCardsInfo = await fetch(`${SSR_API}/cards.json`)
  const cardsInfo = await fetchedCardsInfo.json()
  let map = {
    '/': { page: '/' },
    '/about': { page: '/about' },
    '/links': { page: '/links' }
  }
  cardsInfo.forEach((c) => {
    map[`/${c.type}/${c.id}`] = {
      page: '/postpage',
      query: { type: c.type, id: c.id }
    }
  })
  return map
}

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
})

module.exports = withSass(withCSS(withBundleAnalyzer({
  sassLoaderOptions: {
    includePaths: ['./node_modules'],
    implementation: require('sass')
  },

  webpack: (config, options) => {
    config.plugins.push(new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(PRODUCTION),
      CSR_API: JSON.stringify(CSR_API),
      SSR_API: JSON.stringify(SSR_API)
    }))

    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    })
    return config
  },

  exportPathMap
})))