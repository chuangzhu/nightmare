# Nightmare ![version](https://img.shields.io/badge/version-0.3.7-blue.svg)

A simple blog engine built on React.

![Nightmare Demo](https://i.loli.net/2019/11/24/NhEWqsVKw7rfAte.png)

## Usage

### Prerequisite

* A server running Linux.

### Prepare your post repo

Create a git repository with this structure (see [Format](#configuration-format)):

```
.
├── blog
│   ├── blog-1.md
│   └── blog-2.md
├── note
│   ├── note-1.md
│   └── note-2.md
├── about.toml
├── config.toml
└── links.toml
```

### Install nightmare

Install these dependencies:

```
git
python3 (>= 3.5)
python3-pip
nodejs (with npm)
```

Clone nightmare repo:

```sh
git clone https://gitlab.com/genelocated/nightmare.git /usr/local/lib/nightmare
cd /usr/local/lib/nightmare
pip3 install -r requirements.txt --user
npm install
```

### Setup API server

```sh
cd your-post-repo
/usr/local/lib/nightmare/server.py
```

The API server will start serving on `localhost:5000`. However, `server.py` is just a simple script which don't have an HTTPS encrypting interface. You should shell it with a server software using reversed proxy, for example, [Caddy](https://caddyserver.com):

```caddyfile
https://api.my-blog.com {
  proxy / localhost:5000
  tls me@email.com
  gzip
}
```

### Setup front end server

```sh
cd /usr/local/lib/nightmare
export APIURL=https://api.my-blog.com SSR_API=http://localhost:5000 NODE_ENV=production
npm run build
npm run start
```

Be sure *NOT* to have a `/` at end of the `APIURL` environment variable.

Front end server will start hosting on `localhost:3000`. Likewise, reversed proxy it to a domain:

```caddyfile
https://my-blog.com {
  proxy / localhost:3000
  tls me@email.com
  gzip
}
```

## Export a static site

There is a bash script `scripts/export.sh` for exporting rendered HTML and JavaScript files, so you can deploy anywhere without the requisite of setting up an API server.

```sh
/usr/local/lib/nightmare/scripts/export.sh <post_repo_directory> <export_directory>
```

## Configuration format

### config.toml

```toml
[owner]
nickname = "geneLocated"
fullname = "Zhu Chuang"
avatar = "https://secure.gravatar.com/avatar/bc203266e19187363c0224055d0ce6d7"

  [owner.social]
  twitter = "https://twitter.com/genelocated"
  gitlab = "https://gitlab.com/genelocated"

[site]
cname = "my-blog.com"
name = "MeltyLand of geneLocated"
title = "MELTYLAND"
sub = "of geneLocated"

[disqus]
shortname = "meltyland"

[license]
default = "cc-by-sa"	
```

* `owner.social`: An table of your social account links, each key is the icon of the social media brand in [Material Design Icons](https://materialdesignicons.com/).

* `disqus`: (optional) Leave blank if you are unwilling to use Disqus.

* `license`: Lower case. Support all Creative Commons licenses and Public Domain (`pd`). Using a supported license it will show the license icon in post pages, elsewhere it shows the name of your license.

### about.toml

You can introduce many things in `about.toml`: not only yourself, but also the blog, the license, and even your pet (aka. “aboutees”). You can arrange each “aboutees” a bubble to introduce. Each bubble is described in this format:

```toml
[me]
name = "Zhu Chuang"
avatar = "https://secure.gravatar.com/avatar/bc203266e19187363c0224055d0ce6d7"
hero_img = 'https://i.loli.net/2019/04/27/5cc3f83b4819c.jpg'
intro = '''
## Who am I?
I am Zhu Chuang. `geneLocated` is my nickname.
'''
```

* `hero_img`: (optional)
* `intro`: Introduction text in Markdown.

![about.toml Demo](https://i.loli.net/2020/03/24/SsRP5DgcFQiZaWT.png)

### links.toml

Configuration to place your friend links. Format:

```toml
[friend-1]
url = "https://friend-1.com"
title = "Friend 1"
avatar = "https://"
intro = '''
An undergraduate classmate of mine, majored in Electrical Engineering.
'''

[friend-2]
...
```

### Configuration for each post (optional)

A code box with the “language” `page.ini` is considered as the configuration of current post.

~~~markdown
```page.ini
created = 1537106106
modified = 1540658110
redirect = /another-page
title = My Awesome Title
license = MIT
```
~~~

- `created`: Created time in UNIX time stamp.
- `modified`: Modified time in UNIX time stamp. If it is not set, Nightmare will sort the posts using the modified time told by git.
- `redirect`: If you moved a post to another URL, specify this property to make the original page redirect to the new URL.
- `title`: Nightmare will use the headline in the post as title by default. Specify another title here.
- `license`: You can specify a license that is different from the default one.
