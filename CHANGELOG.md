## v0.3.7

* `Added` syntax highlight for shell `session`s.
* `Security` fix a webhook security issue.
* `Fixed` styles.

## v0.3.6

* `Changed` the icon packs from [Google's material design icons](https://material.io/resources/icons/) and [Font Awesome](https://fontawesome.com/) to [Templarian's material design icons](https://materialdesignicons.com/). This is a breaking change, meaning that you should change your `config.toml` according to the new README.
* `Added` GitLab webhook.

### Performance

* `Changed` the backend server implementation from `SimpleHttpRequestHandler` to Tornado.
* `Changed`. Stylesheets are now loaded using `<link>`.

### Appearance

* `Added` created and modified date chips on index page.
* `Changed` the text in post cards from 8 lines to 4 lines.
* `Added` an animation on LazyLoadImage loading.

## v0.3.5

* `Changed` mermaid, KaTeX and code blocks now have their own components.
* `Security` update dependencies.

## v0.3.4

* `Changed` how about bubbles are rendered, you don't need `hero_color` in `about.toml` anymore.

## v0.3.3-hotfix1

* `Fixed` API data caches on server side.

## v0.3.3

### Features

* `Changed` allowing no introduction to friends.
* `Changed` lazy loading only when it's scrolled.

### Performance

* `Changed` caching API data using global `getInitialProps` context.
* `Fixed` the bug that lazy loading image loads before script is loaded.
* `Added` lazy loading for friend links page.

### Other

* `Changed` markdown parser to `react-markdown` (Partially, still uses `marked` to generate table of contents).
* `Removed` unused dependencies.
* `Added` more ripples.
* `Added` loading indicator.

## v0.3.2

### Features

* `Added` RSS feed.
* `Fixed` `script/export.sh` because we added RSS feed and thumbnail image.

### UX

* `Changed` outline card from `<details>` to material button.
* `Fixed` Disqus card style.
* `Added` elevation increases to Top app bars upon scroll.
* `Fixed` avatar style of about page.
* `Added` more ripples.
* `Added` dark theme.
  
  ![Dark Theme](https://i.loli.net/2019/11/24/NhEWqsVKw7rfAte.png)

### Performance

* `Added` caching fetched initial props.
* `Changed` math equation renderer from MathJax to KaTeX.

## v0.3.1

### Front-end

* `Added` lazy load image for index page.
* `Fixed` style of ``[`Monospaced link`](https://example.org)``.
* `Added` a bash script `scripts/export.sh` for exporting a static site.
* `Removed` some duplicated stylesheet.

### Back-end (API)

* `Added` thumbnail images in `/cards.json`.
