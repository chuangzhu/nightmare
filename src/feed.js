const RSS = require('rss')
const fetch = require('isomorphic-unfetch')

const APIURL = process.env.APIURL || 'http://localhost:5000'
const SSR_API = process.env.SSR_API || APIURL

async function generateRSS() {
  const fetchedBlogConfig = await fetch(`${SSR_API}/config.json`)
  const blogConfig = await fetchedBlogConfig.json()
  let feed = new RSS({
    title: blogConfig.site.name,
    feed_url: `//${blogConfig.site.cname}/feed.xml`,
    site_url: `//${blogConfig.site.cname}`
  })

  const fetchedCardsInfo = await fetch(`${SSR_API}/cards.json`)
  const cardsInfo = await fetchedCardsInfo.json()
  for (let i = 0; i < 10; i++) {
    let c = cardsInfo[i]
    feed.item({
      title: c.title,
      description: c.brief,
      url: `//${blogConfig.site.cname}/${c.type}/${c.id}`,
      guid: `${c.type}/${c.id}`,
      date: new Date(c.modified * 1000)
    })
  }

  return feed.xml()
}

module.exports = generateRSS
