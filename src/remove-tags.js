
module.exports = (string) => (
    string.replace(/<[^ ][^>]*>/g, '')
      .replace(/<\/[^>]*>/g, '')
)