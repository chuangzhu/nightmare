import marked from 'marked'
import escapeHTML from 'escape-html'
import ini from 'ini'
import removeTags from './remove-tags'

/**
 * Convert Markdown string to an Object with result HTML and post config.
 */

function myMarked(markdown) {
  const defaultRenderer = new marked.Renderer()
  let myRenderer = new marked.Renderer()

  let config = null
  let headings = []
  let title = null

  myRenderer.code = (code, language) => {
    switch (language) {
      case 'page.ini':
        config = ini.parse(code)
        return ''
      case 'mermaid':
        return '<div class="mermaid">' +
          escapeHTML(code) +
          '</div>'
      default:
        return defaultRenderer.code(code, language)
    }
  }

  myRenderer.table = (header, body) => {
    // make table horizontally scrollable
    return '<div class="table-container">' +
      defaultRenderer.table(header, body) +
      '</div>'
  }

  myRenderer.heading = (text, level, raw, slugger) => {
    if (level == 1 && !title) {
      title = removeTags(text)
    }
    const slug = slugger.slug(raw)
    headings.push({
      level,
      text: removeTags(text),
      slug
    })
    return `<h${level} id="${slug}">${text}</h${level}>\n`
  }

  myRenderer.image = (href, title, text) => {
    if (href === null) {
      return text
    }

    var out = '<img src="' + href + '" alt="' + text + '"'
    if (title) {
      out += ' title="' + title + '"'
    }
    out += ' loading="lazy"'
    out += '/>'
    return out
  }

  const html = marked(markdown, { renderer: myRenderer })

  return {
    html,
    config,
    headings,
    title
  }
}

export default myMarked