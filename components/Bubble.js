import Img from './LazyLoadImage'

const bubbleStyle = {
  position: 'relative',
  marginBottom: '1em',
  borderTopLeftRadius: 0
}

const heroImageStyle = {
  width: 'calc(100% + 16px)',
  clipPath: 'polygon(0 0, 16px 16px, 16px 100%, 100% 100%, 100% 0%)',
  left: -16,
  borderTopLeftRadius: 0
}

const Bubble = (props) => (
  <div className="mdc-card bubble" style={bubbleStyle}>
    {props.heroImage ?
      <Img className="mdc-card__media" src={props.heroImage} style={heroImageStyle} />
      : null}
    {props.children}
    <style jsx>{`
      .bubble:before {
        content: "";
        width: 0px;
        height: 0px;
        position: absolute;
        border-left: 8px solid transparent;
        border-bottom: 8px solid transparent;
        border-right: 8px solid var(--mcolor);
        border-top: 8px solid var(--mcolor);
        left: -16px;
        top: 0px;
      }
    `}</style>
  </div>
)

export default Bubble
