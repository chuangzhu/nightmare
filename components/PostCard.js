
import Link from 'next/link'
import Card from '@material/react-card'
import { Headline6, Subtitle2, Body2 } from '@material/react-typography'
import CardMedia from './LazyLoadCardMedia'
import CardPrimaryContent from './CardPrimaryContent'
import Chip from './Chip'

const primaryStyle = {
  padding: '1rem'
}

const titleStyle = {
  margin: 0
}

const bodyStyle = {
  opacity: 0.57
}

function getDateString(timestamp) {
  // JS timestamp = UNIX timestamp * 1000
  const d = new Date(timestamp * 1000)
  return d.toLocaleDateString()
}

const PostCard = props => (
  <Card className={"mdc-card post-card mdc-layout-grid__cell " + `mdc-layout-grid__cell--span-${props.info.cols}`}>

    {props.info.image && <CardMedia wide imageUrl={props.info.image} thumbnail={props.info.thumbnail} />}

    <Link
      href={props.info.redirect || `/postpage?id=${props.info.id}&type=${props.info.type}`}
      as={props.info.redirect ? undefined : `/${props.info.type}/${props.info.id}`}
      passHref>

      <CardPrimaryContent style={primaryStyle}>
        <Headline6 style={titleStyle}>{props.info.title}</Headline6>
        <div style={{
          padding: 4,
          display: 'flex',
          flexWrap: 'wrap'
        }}>
          <Chip icon="label" label={props.info.type} title="Category" />
          <Chip icon="update" label={getDateString(props.info.modified)} title="Updated date" />
          <Chip icon="note-plus" label={getDateString(props.info.created)} title="Created date" />
        </div>
        <div
          className="mdc-typography--body2"
          style={bodyStyle}
          dangerouslySetInnerHTML={{ __html: props.info.brief }} />
      </CardPrimaryContent>

    </Link>

  </Card>

)

export default PostCard
