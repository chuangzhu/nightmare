
import React, { Component } from 'react'
import Link from 'next/link'
import Button from '@material/react-button'
import { Headline4 } from '@material/react-typography'

const licenseBarStyle = {
  fontSize: 24,
  fontWeight: 100,
  margin: 0,
  textDecoration: 'none',
  color: 'var(--fgcolor)',
  padding: '0 10px 0 10px'
}

const copyrightInfoStyle = { opacity: 0.57, paddingLeft: 10 }

class Footer extends Component {
  render() {
    const { config, postConfig } = this.props
    const { owner } = config
    const license = postConfig ? (postConfig.license || config.license.default) : config.license.default
    const licenseIcons = this.getLicenseIcons(license)
    const year = (new Date()).getFullYear()

    return (
      <footer className="my-footer">
        <style jsx>{`
          .license-icon { margin-right: 5px; }
          .license-icon:last-child { margin-right: 0; }
        `}</style>

        <Link href="/about#copyright" passHref>
          <Button style={licenseBarStyle}>
            {licenseIcons.length ?
              licenseIcons.map(icon =>
                <span key={icon} className="license-icon"><i className={`mdi mdi-${icon}`} /></span>
              ) :

              <Headline4 style={{ margin: 0 }}>{license.toUpperCase()}</Headline4>}
          </Button>
        </Link>
        <div style={copyrightInfoStyle}>Copyright {year} {owner.fullname}</div>
      </footer>
    )
  }

  getLicenseIcons(license) {
    switch (license) {
      case 'cc0':
        return ['creative-commons-zero']
      case 'pd':
        return ['creative-commons-pd']
    }
    if (/^cc-/.test(license)) {
      const signatures = license.split('-')
      let icons = []
      for (let s of signatures) {
        switch (s) {
          case 'cc':
            icons.push('creative-commons')
            break
          case 'by':
            icons.push('creative-commons-by')
            break
          case 'nc':
            icons.push('creative-commons-nc')
            break
          case 'sa':
            icons.push('creative-commons-sa')
            break
          case 'nd':
            icons.push('creative-commons-nd')
            break
        }
      }
      return icons
    }
    return []
  }
}

export default Footer