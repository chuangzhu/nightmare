
import React, { Component } from 'react'
import Card from '@material/react-card'
import Button from '@material/react-button'
import LinearProgress from '@material/react-linear-progress'
import { DiscussionEmbed } from 'disqus-react'


class DisqusCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comment: false,
      loaded: false
    }
    this.initDisqus = this.initDisqus.bind(this)
    this.showComment = this.showComment.bind(this)
  }

  mutationObserver = null

  render() {
    const { shortname } = this.props.config.disqus
    const disqusConfig = {
      url: `http://${this.props.config.site.cname}/${this.props.slug}`,
      identifier: this.props.slug,
      title: this.props.title
    }
    return (
      <Card className="disqus-card">
        {this.state.comment ?
          <div className="disqus-inner" ref={this.initDisqus}>
            <DiscussionEmbed shortname={shortname} config={disqusConfig} />
            {this.state.loaded ? null : <LinearProgress indeterminate />}
          </div> :
          <Button onClick={this.showComment} style={{ height: 50 }}>Show Comment</Button>}

        <style jsx>{`
          .disqus-inner {
            margin: .5rem;
          }
          @media screen and (min-width: 600px) {
            .disqus-inner {
              margin: 1rem;
            }
          }
        `}</style>
      </Card>
    )
  }

  showComment() {
    this.setState({ comment: true })
  }

  initDisqus(el) {
    // componentWillUnmount
    if (el === null) {
      if (this.mutationObserver)
        this.mutationObserver.disconnect()
      return
    }

    const observerCallback = (mutations, observer) => {
      for (let mutation of mutations) {
        if (mutation.target.title === 'Disqus') {
          this.setState({ loaded: true })
          observer.disconnect()
        }
      }
    }
    this.mutationObserver = new MutationObserver(observerCallback)
    this.mutationObserver.observe(el, { attributes: true, childList: true, subtree: true })
  }
}

export default DisqusCard
