import React, { Component } from 'react'
import Card, { CardPrimaryContent, CardActionIcons } from '@material/react-card'

const linkStyle = level => ({
  marginLeft: `${2 * (level - 1)}em`,
  color: 'var(--link-color)',
  textDecoration: 'none',
  whiteSpace: 'nowrap'
})

const primaryStyle = {
  padding: 10,
  display: 'flex',
  flexDirection: 'row',
}

const detailsStyle = { padding: '0 10px 10px 10px', overflow: 'auto' }

/* props.headings: [{ level, text, slug }...] */
class TableOfContents extends Component {
  state = {
    open: true
  }

  render() {
    let links = []
    for (let heading of this.props.headings) {
      const { level, text, slug } = heading
      links.push(
        <p key={slug} style={{ margin: 0 }}>
          <a href={`#${slug}`} style={linkStyle(level)}>{text}</a>
        </p>
      )
    }
    return (
      <Card className="outline-card">
        <CardPrimaryContent
          style={primaryStyle}
          onClick={() => this.setState({ open: !this.state.open })}>
          <h1>Outline</h1>
          <CardActionIcons>
            <span className={'mdc-icon-button__icon mdi mdi-' +
              (this.state.open ? 'chevron-up' : 'chevron-down')} />
          </CardActionIcons>
        </CardPrimaryContent>
        {this.state.open ?
          <div style={detailsStyle}>
            {links}
          </div> : null}
      </Card>
    )
  }

  componentDidMount() {
    if (window.innerWidth <= 600) {
      this.setState({ open: false })
    }
  }
}

export default TableOfContents
