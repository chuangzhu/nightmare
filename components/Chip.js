import React from 'react'

const chipStyle = {
  margin: 4,
  padding: '7px 12px',
  display: 'inline-flex',
  borderRadius: 16,
  fontSize: '0.875rem',
  lineHeight: '1.25rem',
  fontWeight: 400,
  letterSpacing: '0.01786em',
  height: 32,
  alignItems: 'center',
  boxSizing: 'border-box',
  width: 'min-content',
}

const iconStyle = {
  margin: '-4px 4px -4px -4px',
  width: 20,
  height: 20,
  fontSize: 20
}

const Chip = ({ icon, label, ...otherProps }) => (
  <div style={chipStyle} className="chip" {...otherProps}>
    <span className={`mdi mdi-${icon}`} style={iconStyle} />
    <div style={{ whiteSpace: "nowrap" }}>{label}</div>
    <style jsx>{`
      .chip { background-color: #e7e7e7 }
      @media (prefers-color-scheme: dark) {
        .chip { background-color: #333 }
      }
    `}</style>
  </div >
)

export default Chip
