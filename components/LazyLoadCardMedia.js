import React, { Component } from 'react'
import classnames from 'classnames'
import { CSS_CLASSES } from '@material/react-card/constant.ts'

const mediaStyle = (imageUrl) => ({
  backgroundImage: `url(${imageUrl})`
})

class CardMedia extends Component {
  isMounted = false
  constructor(props) {
    super(props)
    this.state = {
      style: this.props.thumbnail &&
        mediaStyle(`data:image/png;base64,${this.props.thumbnail}`),
      hasScript: false,
      el: null
    }
    this.initListener = this.initListener.bind(this)
    this.scrollListener = this.scrollListener.bind(this)
    this.loadImage = this.loadImage.bind(this)
  }

  render() {
    const variant = {
      // append '--square' only when props.square is true
      [CSS_CLASSES.MEDIA_SQUARE]: this.props.square,
      [CSS_CLASSES.MEDIA_16_9]: this.props.wide
    }
    const containerClasses = classnames('mdc-card__media-container', this.props.className, variant)
    const mediaClasses = classnames(CSS_CLASSES.MEDIA, variant)

    return (
      this.state.hasScript ?

        <div
          className={mediaClasses}
          style={{ ...(this.state.style), ...(this.props.style) }}
          ref={this.initListener} /> :

        // noscript fallback, user with script enabled won't load inner image
        <div className={containerClasses}>
          <noscript className={containerClasses}>
            <div
              className={mediaClasses}
              loading="lazy"
              style={{ ...mediaStyle(this.props.imageUrl), ...(this.props.style) }} />
          </noscript>
        </div>
    )
  }

  componentDidMount() {
    this.isMounted = true
    this.setState({ hasScript: true })
  }

  initListener(el) {
    // el is null <==> componentWillUnmount
    if (el === null) {
      this.isMounted = false
      window.removeEventListener('scroll', this.scrollListener)
      return
    }

    const rect = el.getBoundingClientRect()
    if (rect.top < window.innerHeight) {
      this.loadImage()
    } else {
      this.state.el = el
      window.addEventListener('scroll', this.scrollListener)
    }
  }

  scrollListener() {
    const rect = this.state.el.getBoundingClientRect()
    if (rect.top < window.innerHeight) {
      this.loadImage()
      window.removeEventListener('scroll', this.scrollListener)
    }
  }

  loadImage() {
    const imageLoader = new Image()
    imageLoader.onload = () => {
      if (this.isMounted)
        this.setState({ style: mediaStyle(this.props.imageUrl) })
    }
    imageLoader.src = this.props.imageUrl
  }
}

export default CardMedia