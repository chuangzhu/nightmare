
import React, { Component } from 'react'
import Link from 'next/link'
import Button from '@material/react-button'
import IconButton from '@material/react-icon-button'

import Drawer, { DrawerContent, DrawerAppContent } from '@material/react-drawer'
import TopAppBar, {
  TopAppBarFixedAdjust,
  TopAppBarIcon,
  TopAppBarRow,
  TopAppBarSection
} from '@material/react-top-app-bar'
import List, { ListItem, ListItemText, ListItemGraphic } from '@material/react-list'

import Router from 'next/router'
import LinearProgress from '@material/react-linear-progress'


const MyDrawerItem = ({ href, where, icon, children }) => (
  <Link href={href}>
    <ListItem
      activated={where === href}>
      <ListItemGraphic graphic={
        // Wrapping with an element is required to show the icon
        <span>
          <span className={`mdi mdi-${icon}`} />
        </span>
      } />
      <ListItemText primaryText={children} />
    </ListItem>
  </Link>
)

const siteTitleStyle = {
  display: 'flex',
  color: 'inherit',
  textTransform: 'none',
  textDecoration: 'none'
}

const siteSubtitleStyle = {
  color: 'inherit',
  opacity: 0.75,
  paddingLeft: '0.5rem',
  paddingRight: 20,
  fontFamily: 'Damion, cursive'
}

class MyHeader extends Component {
  state = {
    open: false,
    shadow: false,
    loading: false
  }

  render() {
    const { title, sub } = this.props.config.site

    let { shadow } = this.state
    if (process.browser)
      shadow = window.scrollY > 56

    return (
      <div>
        <Drawer modal open={this.state.open} onClose={() => this.setState({ open: false })}>
          <DrawerContent tag='main'>
            <List>
              <MyDrawerItem href="/" icon="home" where={this.props.here}>Home</MyDrawerItem>
              <MyDrawerItem href="/about" icon="information" where={this.props.here}>About</MyDrawerItem>
              <MyDrawerItem href="/links" icon="link" where={this.props.here}>Links</MyDrawerItem>
              <MyDrawerItem href="/feed.xml" icon="rss" where={this.props.here}>Feed</MyDrawerItem>
            </List>
          </DrawerContent>
        </Drawer>

        <DrawerAppContent>
          <TopAppBar className={shadow ? 'mdc-elevation--z4' : null}>
            <TopAppBarRow>
              <TopAppBarSection align='start'>
                <TopAppBarIcon navIcon tabIndex={0}>
                  {/* Wrap icon in IconButton to prevent :before rewriting */}
                  <IconButton
                    onClick={() => this.setState({ open: !this.state.open })}>
                    <span className="mdi mdi-menu" />
                  </IconButton>
                </TopAppBarIcon>
                <Link href="/" passHref>
                  <Button style={siteTitleStyle}>
                    <span className="mdc-top-app-bar__title">{title}</span>
                    <span className="mdc-top-app-bar__title" style={siteSubtitleStyle}>{sub}</span>
                  </Button>
                </Link>
              </TopAppBarSection>
            </TopAppBarRow>

            {this.state.loading && <LinearProgress indeterminate />}
          </TopAppBar>
          <TopAppBarFixedAdjust />
        </DrawerAppContent>
      </div >
    )
  }

  constructor(props) {
    super(props)
    this.eventListener = this.eventListener.bind(this)
  }

  eventListener() {
    this.setState({ shadow: window.scrollY > 56 })
  }

  componentDidMount() {
    window.addEventListener('scroll', this.eventListener)

    Router.onRouteChangeStart = () => this.setState({ loading: true })
    Router.onRouteChangeComplete = () => this.setState({ loading: false })
    Router.onRouteChangeError = () => this.setState({ loading: false })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.eventListener)
  }
}

export default MyHeader
