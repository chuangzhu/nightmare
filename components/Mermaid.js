import React from 'react'

export default ({ value }) => {
  const [svg, setSvg] = React.useState(null)
  React.useEffect(() => {
    (async function() {
      if (window.mermaid === undefined) {
        window.mermaid = await import('mermaid')
        window.mermaid.mermaidAPI.initialize({ startOnLoad: false })
      }
      window.mermaid.mermaidAPI.render(
        // believe it or not,
        // this is exactly what mermaid does in mermaid.init()
        `mermaid-${Date.now()}`,
        value,
        s => setSvg(s)
      )
    })()
  }, [])

  if (svg) return (
    <div className="mermaid" dangerouslySetInnerHTML={{ __html: svg }} />
  )
  return (
    <pre title="Enable JavaScript to render this mermaid graph">
      {value}
    </pre>
  )
}
