
import React, { Component } from 'react'
import Link from 'next/link'

const isObject = (value) => value !== null && typeof value === 'object'

class ProfileCard extends Component {
  render() {
    const { owner } = this.props.config
    let socialIcons = []
    for (let i in owner.social) {
      if (isObject(owner.social[i])) {
        socialIcons.push(
          <a href={owner.social[i].addr} key={i}>
            <i className={owner.social[i].icon} />
          </a>
        )
      } else {
        socialIcons.push(
          <a href={owner.social[i]} key={i}>
            <i className={`mdi mdi-${i}`} />
          </a>
        )
      }
    }
    return (
      <div className="profile-card mdc-card">
        <Link href="/about">
          <a><img className="avatar" src={owner.avatar} loading="lazy" /></a>
        </Link>
        <h1>About Me</h1>
        <h2 className="social-icons">
          {socialIcons}
        </h2>

        <style jsx>{`
          .avatar {
            width: 100%;
          }
          @media screen and (max-width: 600px) {
            .avatar {
              width: 60%;
              display: table;
              margin: 1em auto;
              border-radius: 50%;
              box-shadow: 20px 20px 80px #888;
            }
          }
        `}</style>
      </div>
    )
  }
}

export default ProfileCard