
import React, { Component } from 'react'
import Markdown from './Markdown'
import Card, { CardActionIcons } from '@material/react-card'
import CardMedia from './LazyLoadCardMedia'
import { Headline6, Subtitle2 } from '@material/react-typography'
import CardPrimaryContent from './CardPrimaryContent'


const avatarStyle = {
  width: 110,
  borderTopRightRadius: 0
}
const primaryStyle = {
  display: 'flex',
  flexDirection: 'row',
}
const entryStyle = { padding: '1em' }
const titleStyle = { margin: 0 }
const subtitleStyle = {
  opacity: 0.57,
  margin: 0
}
const articleStyle = { padding: '1em 3%' }



class FriendCard extends Component {
  state = {
    open: true
  }

  render() {
    const { avatar, url, title } = this.props.info
    return (
      <Card className="mdc-layout-grid__cell--span-12">
        <div className="mdc-card__primary-action" style={primaryStyle}>
          {avatar &&
            <CardMedia square style={avatarStyle} imageUrl={avatar} />}

          <CardPrimaryContent style={entryStyle} href={url}>
            <Headline6 style={titleStyle}>{title}</Headline6>
            {this.props.info.slogan &&
              <Subtitle2 style={subtitleStyle}>{this.props.info.slogan}</Subtitle2>}
            <Subtitle2 style={subtitleStyle}>{url.split('//')[1]}</Subtitle2>
          </CardPrimaryContent>

          {this.props.info.intro &&
            <CardActionIcons>
              <button className="mdc-icon-button"
                aria-pressed="false" aria-label="Expand intro" title="Expand intro"
                onClick={() => this.setState({ open: !this.state.open })}>
                <span className={'mdc-icon-button__icon mdi mdi-' +
                  (this.state.open ? 'chevron-up' : 'chevron-down')} />
              </button>
            </CardActionIcons>}
        </div>

        {this.state.open && this.props.info.intro ?
          <div style={articleStyle}><Markdown>{this.props.info.intro}</Markdown></div> : null}
      </Card>
    )
  }

  componentDidMount() {
    this.setState({ open: false })
  }
}

export default FriendCard

