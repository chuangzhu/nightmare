
import React from 'react'
import prism from 'prismjs'
import 'prismjs/components/prism-go.min.js'
import 'prismjs/components/prism-bash.min.js'
import '../src/prism-session.js'
import 'prismjs/components/prism-python.min.js'
import 'prismjs/components/prism-clike.min.js'
import 'prismjs/components/prism-javascript.min.js'
import 'prismjs/components/prism-makefile.min.js'
import 'prismjs/components/prism-nix.min.js'
prism.languages.sh = prism.languages.bash
prism.languages.cpp = prism.languages.clike
prism.languages.c = prism.languages.clike

export default function CodeBlock({ language, value }) {
  function highlight(el) {
    if (el)
      prism.highlightElement(el)
  }
  return (
    <pre
      ref={language ? highlight : undefined}
      className={language ? `language-${language}` : undefined}>
      <code>{value}</code>
    </pre>
  )
}
