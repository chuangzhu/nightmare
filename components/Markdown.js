
import ReactMarkdown, {
  renderers as defaultRenderers
} from 'react-markdown'
import Slugger from '../src/Slugger.js'
import MathPlugin from 'remark-math'
import Img from './LazyLoadImage'
import { InlineMath, BlockMath } from './KaTeX'
import CodeBlock from './CodeBlock'
import Mermaid from './Mermaid'

// extract inner text from props of an element
const getRaw = elementProps => {
  if (typeof elementProps.children === "string") {
    return elementProps.children
  }
  let raw = ''
  for (let child of elementProps.children) {
    raw += getRaw(child.props)
  }
  return raw
}

// custom react-markdown renderers
const renderers = slugger => ({
  code: (props) => {
    switch (props.language) {
      case 'page.ini':
        return null
      case 'mermaid':
        return <Mermaid value={props.value} />
      default:
        return <CodeBlock {...props} />
    }
  },

  image: props => (
    <Img alt={props.alt} src={props.src} />
  ),

  table: (props) => (
    // make table horizontally scrollable
    <div className="table-container">
      {defaultRenderers.table(props)}
    </div>
  ),

  heading: (props) => {
    const { level, children } = props
    return React.createElement(
      `h${level}`,
      { id: slugger.slug(getRaw(props)) },
      children
    )
  },

  inlineMath: ({ value }) => <InlineMath math={value} />,
  math: ({ value }) => <BlockMath math={value} />
})

const Markdown = props => {
  // one slugger instance each <Markdown>, do not reuse it
  const slugger = new Slugger()
  return (
    <article
      className="article">
      <ReactMarkdown
        source={props.children}
        escapeHtml={false}
        plugins={[MathPlugin]}
        renderers={renderers(slugger)} />
    </article>
  )
}

export default Markdown
