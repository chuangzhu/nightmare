import React, { Component } from 'react'

const fileIconStyle = {
  verticalAlign: 'calc(0px - (24px - var(--font-size)) / 2 + 2px)'
}

const loadingIconStyle = {
  marginRight: '.4rem',
  ...fileIconStyle
}

const LoadingPlaceholder = ({ alt }) => (
  <>
    <span className="mdi mdi-file-image" style={fileIconStyle} />
    <span className="mdi mdi-loading mdi-spin" style={loadingIconStyle} />
    {alt && <span>{alt}</span>}
  </>
)

const placeholderStyle = {
  display: 'block',
  textAlign: 'center'
}

class Img extends Component {
  el = null
  constructor(props) {
    super(props)
    this.state = {
      src: this.props.thumbnail && `data:image/png;base64,${this.props.thumbnail}`,
      hasScript: false
    }
    this.initListener = this.initListener.bind(this)
    this.scrollListener = this.scrollListener.bind(this)
    this.loadImage = this.loadImage.bind(this)
  }

  render() {
    const { src: dataSrc, ...otherProps } = this.props
    return (
      this.state.hasScript ?
        (this.state.src ?
          <img
            src={this.state.src}
            ref={this.initListener}
            {...otherProps} /> :
          <span ref={this.initListener} style={placeholderStyle}>
            <LoadingPlaceholder {...otherProps} />
          </span>) :

        <noscript>
          <img
            src={dataSrc}
            loading="lazy"
            {...otherProps} />
        </noscript>
    )
  }

  componentDidMount() {
    this.setState({ hasScript: true })
  }

  initListener(el) {
    // el is null <==> componentWillUnmount
    if (el === null) {
      window.removeEventListener('scroll', this.scrollListener)
      return
    }

    const rect = el.getBoundingClientRect()
    if (rect.top < window.innerHeight) {
      this.loadImage()
    } else {
      this.el = el
      window.addEventListener('scroll', this.scrollListener)
    }
  }

  scrollListener() {
    const rect = this.el.getBoundingClientRect()
    if (rect.top < window.innerHeight) {
      this.loadImage()
      window.removeEventListener('scroll', this.scrollListener)
    }
  }

  loadImage() {
    const imageLoader = new Image()
    imageLoader.onload = () => {
      this.setState({ src: this.props.src })
    }
    imageLoader.src = this.props.src
  }
}

export default Img
