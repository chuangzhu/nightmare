import KaTeX from 'katex'

export function BlockMath({ math }) {
  const __html = KaTeX.renderToString(math, {
    displayMode: true,
    throwOnError: false
  })
  return <div dangerouslySetInnerHTML={{ __html }} />
}

export function InlineMath({ math }) {
  const __html = KaTeX.renderToString(math, {
    displayMode: false,
    throwOnError: false
  })
  return <span dangerouslySetInnerHTML={{ __html }} />
}
