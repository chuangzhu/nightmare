
import React, { Component } from 'react'
import Head from 'next/head'
import Header from './Header'

class Layout extends Component {
  state = { open: false }

  render() {
    const { name } = this.props.config.site
    return (
      <div>
        <Head>
          <title>{this.props.title} - {name}</title>
          <meta name="theme-color" content="#efefef" />
        </Head>

        <Header here={this.props.here} config={this.props.config} />
        <div className="main-content">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Layout