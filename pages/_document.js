import React from 'react'
import Document, {
  Html,
  Head,
  Main,
  NextScript
} from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link rel="stylesheet" type="text/css"
            href={`${CSR_API}/webfont/materialdesignicons.min.css`} />
          <link rel="stylesheet" type="text/css"
            href="https://fonts.googleapis.com/css?family=Damion&display=swap" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
