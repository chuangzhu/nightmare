import React from 'react'
import App from 'next/app'

import '../src/app.scss'
import '../src/article.scss'

let apiCache = { articles: {} }

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return <Component {...pageProps} />
  }

  static async getInitialProps({ Component, ctx }) {
    if (process.browser) {
      ctx.apiCache = apiCache
    } else {
      ctx.apiCache = { articles: {} }
    }

    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return { pageProps }
  }
}

export default MyApp
