
import React, { Component } from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from '../components/Layout'
import FriendCard from '../components/FriendCard'
import APIURL from '../src/apiurl'

class LinksPage extends Component {
  render() {
    const { linksConfig, config } = this.props
    let friends = []
    for (let c in linksConfig) {
      friends.push(
        <FriendCard info={linksConfig[c]} key={c} />
      )
    }
    return (
      <Layout title="Friend Links" here="/links" config={config}>
        <div className="mdc-layout-grid main-content">
          <div className="mdc-layout-grid__inner">
            {friends}
          </div>
        </div>
      </Layout>
    )
  }

  static async getInitialProps({ apiCache }) {
    if (!('config' in apiCache)) {
      const fetchedConfig = await fetch(`${APIURL}/config.json`)
      apiCache.config = await fetchedConfig.json()
    }
    if (!('linksConfig' in apiCache)) {
      const fetched = await fetch(`${APIURL}/links.json`)
      apiCache.linksConfig = await fetched.json()
    }
    return apiCache
  }
}

export default LinksPage
