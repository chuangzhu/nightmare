
import React, { Component } from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from '../components/Layout'
import Bubble from '../components/Bubble'
import Markdown from '../components/Markdown'
import APIURL from '../src/apiurl'

const Aboutee = props => (
  <div className="object" id={props.id} style={{ overflow: 'hidden' }}>
    <img className="avatar mdc-card" src={props.avatar} loading="lazy" />
    <div className="bubble">
      <Bubble heroImage={props.heroImage}>
        <div style={{ padding: '1em 3%' }}>
          <Markdown>{props.children}</Markdown>
        </div>
      </Bubble>
    </div>

    <style jsx>{`
      .bubble {
        float: right;
        width: 80%;
      }
      .avatar {
        border-radius: 100%;
        width: 16%;
        float: left;
      }
      @media screen and (min-width: 600px) {
        .avatar {
          width: 100px;
        }
        .bubble {
          width: calc(100% - 120px);
        }
      }
    `}</style>
  </div>
)

class AboutPage extends Component {
  render() {
    const { aboutConfig, config } = this.props
    let aboutees = []
    for (let c in aboutConfig) {
      aboutees.push(
        <Aboutee
          avatar={aboutConfig[c].avatar}
          heroImage={aboutConfig[c].hero_img}
          id={c}
          key={c}>{aboutConfig[c].intro}</Aboutee>
      )
    }
    return (
      <Layout title="About" here="/about" config={config}>
        <div style={{ padding: '16px' }}>
          {aboutees}
        </div>
      </Layout>
    )
  }

  static async getInitialProps({ apiCache }) {
    if (!('config' in apiCache)) {
      const fetchedConfig = await fetch(`${APIURL}/config.json`)
      apiCache.config = await fetchedConfig.json()
    }
    if (!('aboutConfig' in apiCache)) {
      const fetched = await fetch(`${APIURL}/about.json`)
      apiCache.aboutConfig = await fetched.json()
    }
    return apiCache
  }
}

export default AboutPage
