
import React, { Component } from 'react'
import Layout from '../components/Layout'
import fetch from 'isomorphic-unfetch'
import myMarked from '../src/mymarked'
import Markdown from '../components/Markdown'
import Card from '@material/react-card'
import TableOfContents from '../components/TableOfContents'
import DisqusCard from '../components/DisqusCard'
import ProfileCard from '../components/ProfileCard'
import Footer from '../components/Footer'
import APIURL from '../src/apiurl'

import 'katex/dist/katex.min.css'
import '../src/postpage.scss'

const SlugCard = props => (
  <Card className="slug-card">
    <h1 className="blog-slug">{props.children}</h1>
  </Card>
)

class MainCard extends Component {
  render() {
    return (
      <div className="mdc-card main">
        <Markdown>{this.props.children}</Markdown>
      </div>
    )
  }
}

class PostPage extends Component {
  render() {
    const {
      type,
      id,
      article,
      config: globalConfig
    } = this.props
    const slug = `${type}/${id}`
    // TODO: completely remove marked
    const markedInfo = myMarked(article)
    return (
      <Layout title={markedInfo.title} config={globalConfig}>
        <div className="outer-content">
          <SlugCard>{slug}</SlugCard>

          <TableOfContents headings={markedInfo.headings} />

          <div className="outer-main">
            <MainCard>{article}</MainCard>

            {globalConfig.disqus ?
              <DisqusCard
                config={globalConfig}
                slug={slug}
                title={markedInfo.title} /> : null}
          </div>

          <ProfileCard config={globalConfig} />

          <Footer config={globalConfig} postConfig={markedInfo.config} />
        </div>
      </Layout>
    )
  }

  static async getInitialProps({ query: { type, id }, apiCache }) {
    const slug = `${type}/${id}`
    if (!(slug in apiCache.articles)) {
      const fetchedArticle = await fetch(`${APIURL}/${slug}.md`)
      apiCache.articles[slug] = await fetchedArticle.text()
    }
    if (!('config' in apiCache)) {
      const fetchedConfig = await fetch(`${APIURL}/config.json`)
      apiCache.config = await fetchedConfig.json()
    }
    return {
      type,
      id,
      article: apiCache.articles[slug],
      config: apiCache.config
    }
  }
}

export default PostPage
