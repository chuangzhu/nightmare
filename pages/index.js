
import React, { Component } from 'react'
import { Grid, Row } from '@material/react-layout-grid'
import Layout from '../components/Layout'
import PostCard from '../components/PostCard'
import fetch from 'isomorphic-unfetch'
import APIURL from '../src/apiurl'

class Index extends Component {
  render() {
    return (
      <Layout title="Home" here="/" config={this.props.config}>
        <Grid>
          <Row>
            {this.props.cardsInfo.map((card, i) =>
              <PostCard info={card} key={i} />
            )}
          </Row>
        </Grid>
      </Layout>
    )
  }

  static async getInitialProps({ apiCache }) {
    if (!('cardsInfo' in apiCache)) {
      const fetchedCardsInfo = await fetch(`${APIURL}/cards.json`)
      apiCache.cardsInfo = await fetchedCardsInfo.json()
    }
    if (!('config' in apiCache)) {
      const fetchedConfig = await fetch(`${APIURL}/config.json`)
      apiCache.config = await fetchedConfig.json()
    }
    return apiCache
  }
}


export default Index
