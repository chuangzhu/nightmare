#!/bin/bash

SCRIPT="$0"
SCRIPT_PATH=`dirname "$SCRIPT"`
NIGHTMARE_ROOT=`readlink -f "$SCRIPT_PATH/.."`

print_usage() {
    echo "Usage: $SCRIPT <config_dir> <export_dir>"
}

case $1 in
    -h | --help) print_usage; exit ;;
    "") print_usage; exit 1 ;;
    *) CONFIG_DIR=`readlink -f "$1"` ;;
esac

[[ -n "$2" ]] || {
    print_usage
    exit 1
} 
EXPORT_DIR=`readlink -f "$2"`

# Start API server
cd "$CONFIG_DIR"
python3 "$NIGHTMARE_ROOT/server.py" &
API_SERVER_PID="$!"

for (( ;; )); do
    curl -s http://localhost:5000
    [[ $? = 0 ]] && break
done

export NODE_ENV=production
export APIURL=http://localhost:5000
export SSR_API=http://localhost:5000
export CSR_API=/api

mkdir -p "$EXPORT_DIR"
cd "$NIGHTMARE_ROOT"
./node_modules/.bin/next build
./node_modules/.bin/next export -o "$EXPORT_DIR"
node -e '
const generateRSS = require("./src/feed")
generateRSS().then(rssString => {
  console.log(rssString)
})
' > "$EXPORT_DIR"/feed.xml

mkdir -p "$EXPORT_DIR/api"
curl -fL http://localhost:5000/cards.json -o "$EXPORT_DIR/api/cards.json"
curl -fL http://localhost:5000/config.json -o "$EXPORT_DIR/api/config.json"
curl -fL http://localhost:5000/about.json -o "$EXPORT_DIR/api/about.json"
curl -fL http://localhost:5000/links.json -o "$EXPORT_DIR/api/links.json"
cp -r "$CONFIG_DIR/blog/" "$EXPORT_DIR/api"
cp -r "$CONFIG_DIR/note/" "$EXPORT_DIR/api"
cp -r ~/.cache/nightmare-api/. "$EXPORT_DIR/api"

cp "$NIGHTMARE_ROOT/favicon.ico" "$EXPORT_DIR"

kill "$API_SERVER_PID"
