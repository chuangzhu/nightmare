#!/usr/bin/env node

process.chdir(__dirname)
const express = require('express')
const next = require('next')
const generateRSS = require('./src/feed')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app
  .prepare()
  .then(() => {
    const server = express()
    server.use('/favicon.ico', express.static(__dirname + '/favicon.ico'))

    server.get('/feed.xml', async (req, res) => {
      res.set('Content-Type', 'application/xml')
      res.send(await generateRSS())
    })

    server.get('/:type/:id', (req, res) => {
      const queryParams = {
        id: req.params.id,
        type: req.params.type
      }
      app.render(req, res, '/postpage', queryParams)
    })

    server.get('/:page/', (req, res) => {
      app.render(req, res, `/${req.params.page}`)
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, err => {
      if (err) throw err
      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
